\documentclass{article}

\title{Basic Differential Language}
\author{Robin Cockett and Jonathan Gallagher}


% Bibliography setup 
\usepackage[
    backend=bibtex,
    natbib=true,
    style=authoryear
    ]{biblatex}
    

\addbibresource{references.bib} 

% Packages

% For type inference rules: \infer{judgment}{antecedent & ... & antecedenty}
\usepackage{proof} 

% Standard AMS packages
\usepackage{amsmath,amssymb,amsfonts,amsthm}

% The enumerate packages re-implements enumerate and allows it be be made ``pretty'' \begin{enumerate}[{Item.1}] \item ... will produce Item.1 stuff, Item.2 stuff, etc
\usepackage{enumerate}

% For larger margins
\usepackage{fullpage}

% For prettier typesetting and layout
\usepackage{microtype}

% For rotating images, and other utility graphics stuff
% \usepackage{graphicx}

% For colorizing text
\usepackage{xcolor}

% Nullary commands
\newcommand{\R}{\mathcal{R}}
\newcommand{\proves}{\vdash}
\newcommand{\LT}{\mathsf{L}_T}
\newcommand{\fst}{\mathsf{fst}}
\newcommand{\snd}{\mathsf{snd}}
\newcommand{\ty}{\mathsf{ty}}
\newcommand{\ctx}{\ \mathsf{ctx}}
\newcommand{\fv}{\mathsf{fv}}
\newcommand{\rewrites}{\rightsquigarrow}
\newcommand{\vars}{\mathsf{vars}}
\newcommand{\varsp}{\mathsf{vars}_\mathsf{pat}}
\newcommand{\sdpl}{\mathsf{SDPL}}
\newcommand{\relu}{\mathsf{relU}}
\newcommand{\pident}{\mathsf{PIdent}}
\newcommand{\when}{\spaceit{when}}
\newcommand{\until}{\spaceit{until}}
\newcommand{\doo}{\spaceit{do}}
\newcommand{\inn}{\spaceit{in}}
\newcommand{\lett}{\spaceit{let}}
% Unary commands
\newcommand{\spaceit}[1]{\, \mathsf{#1} \,}
% 4-ary (tetrary?) commands 
\newcommand{\diff}[4]{\frac{\mathsf{d}\, {#1}}{\mathsf{d}\, {#2}}\left({#3};{#4}\right)}


%Theorems
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{proposition}{Proposition}[section]
\newtheorem{corollary}{Corollary}[section]
\begin{document}
\maketitle
This document describes the core differential language $\sdpl$.

\section{Syntax}

In the grammatical rules, 
the notation $[m]$ is used to indicate a list of $m$.

The top level of an $\sdpl$ program consists of a list of definitions which are given by a 
uniform syntax.  

\[
  \mathsf{Prog} \, := \, [\mathsf{Definition}] \quad \mathsf{Main}?
\]

A program may contain an optional definition called ``main'' which is a definition that 
has no arguments -- a program with a main function will run and its final evaluation to 
the console.

\[
  \mathsf{Main} := \texttt{main} \, = \, m    
\]

A definition describes a constant or user defined function.  Definitions are described as

\[
  \mathsf{Definition} := \mathsf{PIdent} \, [\mathsf{Pat}] \, = \, m
\]  

The first $\mathsf{PIdent}$ is the name of the definition, the list of variables patterns 
are formal arguments, and the term $m$ is the body of the definition.

A variable patterns is a nested tuple containing variable names.

\[
 \mathsf{Pat} \, := \, \mathsf{PIdent} \, | \, ( \mathsf{Pat} , \mathsf{Pat}) \, | \, () \, | \, (\mathsf{Pat})
\]

Terms are defined by 

\begin{align*}
    m &:= \, \mathsf{PIdent} \, | \, r \in \mathbb{R} \, | \, 0 \, | \, m + m \, |\, (m) \, | \, () \, |\, (m,m)\\
      &\quad \sin(m) \, | \, \cos(m) \, | \, \exp(m) \, | \, \relu(m) \, | \, m * m \, | \, \pident ([m])\\
      &\quad \when [\mathsf{Case}] \, | \, \lett \, \mathsf{Definition}\, \inn \, m \, | \, \doo [\mathsf{Stmt}] \inn m\\
      &\quad d m / d [\pident] ([m] . [m])
\end{align*}

The $\when$ clause adds control structure based on terms that evaluate as booleans.  
We define the language of booleans as follows:

\begin{align*}
  b &:= \mathsf{true} \, | \, \mathsf{false} \, | \, b \, \&\, b \, | \, b \, || \, b  \, | \, m < m \, | \, m > m
\end{align*}
Note that the double bar $b || b$ is a syntactic construct within $\sdpl$.  Then the clauses of a when construct 
are defined by:

\[
  \mathsf{Case} := b \,\,\, \texttt{->} \,\,\, m    
\]


Finally statements are a way to group local definitions and iterate a group of local definitions.

\begin{align*}
    \mathsf{Stmt} &:= \mathsf{Definition} \\
                  &\quad \doo [\mathsf{Stmt}] \until b
\end{align*}
\subsection{Layout}
At the top-level, definitions are separated formally by semicolons.  However, we also allow 
flush-left layout syntax for a top-level list of definitions -- this means a fully left aligned 
list of definitions need not be terminated by semicolons.

Also, the keywords $\when,\doo$ are layout keywords -- this means that a list of cases or statements 
initiated by $\when$ or $\doo$ formally requires curly braces and semicolon termination; however,
these keywords also serve as layout initializers so that as long as the list of cases or statements 
is indented and aligned the same, then the curly braces and semicolons may be omitted.
For example,
  \begin{verbatim}
    when 
      x<3 -> x*3
      x>16 -> x+y
  \end{verbatim}
Is equivalent to 
  \begin{verbatim}
      when {x < 3 -> x*3; x>16 -> x+y;}
  \end{verbatim}

\subsection{Shadowing}
$\sdpl$ supports variable shadowing, future definitions will overshadow 
existing definitions; variable shadowing can also overwrite the type of the variable being shadowed.

For example, 
\begin{verbatim}
    x = 3 
    x = 5 
    x = (3,4)
\end{verbatim}

is a valid list of statements.  Since $\sdpl$ supports recursive definitions, this 
complicated when a shadowing variable assignment contains itself on the right-hand side.

There are two ways around this.  First we can optionally introduce a new keyword $\mathsf{mut}$ for 
shadowing variable assignments.  For example, 

  \begin{verbatim}
    x = 3
    mut x = (x,x)
  \end{verbatim}

will evaluate to $x$ containing the value $(3,3)$.  The second option is to use a convention
for deciding whether an assignment is overshadowing or recursive and that the convention 
is that overshadowing is chosen if available -- this involves simply knowing what variables 
are defined, and a reassignment is assumed to be shadowing and not recursive.  Under this 
convention the above still evaluates with $x$ containing the value $(3,3)$.  And assuming that $x$
has not yet been defined, then the following specifies a recursive definition:

  \begin{verbatim}
    x = when 
      x < 0 -> 3*x + 1
      x > 0 -> x
  \end{verbatim}

Formal arguments to a definition \emph{always} overshadow any variables with the same name in 
scope.

\subsection{Free variables}
This section defines the set of free variables of a term and definition respectively.

\section{Typing rules}

\section{Substitution}

\section{Source transformation differentiation}


\end{document}