\documentclass{article}

\title{Basic Differential Language}
\author{Robin Cockett and Jonathan Gallagher}

% Packages

% For type inference rules: \infer{judgment}{antecedent & ... & antecedenty}
\usepackage{proof} 

% Standard AMS packages
\usepackage{amsmath,amssymb,amsfonts,amsthm}

% The enumerate packages re-implements enumerate and allows it be be made ``pretty'' \begin{enumerate}[{Item.1}] \item ... will produce Item.1 stuff, Item.2 stuff, etc
\usepackage{enumerate}


% Nullary commands
\newcommand{\R}{\mathcal{R}}
\newcommand{\proves}{\vdash}
\newcommand{\LT}{\mathsf{L}_T}
\newcommand{\fst}{\mathsf{fst}}
\newcommand{\snd}{\mathsf{snd}}
\newcommand{\ty}{\mathsf{ty}}
\newcommand{\ctx}{\ \mathsf{ctx}}
\newcommand{\rewrites}{\rightsquigarrow}
% 4-ary (tetrary?) commands 
\newcommand{\diff}[4]{\frac{\mathsf{d}\, {#1}}{\mathsf{d}\, {#2}}\left({#3};{#4}\right)}


%Theorems
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{definition}{Definition}[section]
\begin{document}
\maketitle

This document gives a description of the language $\mathsf{L}_T$.   

\section{Syntax}
 In the grammatical rules, 
the notation $[m]$ is used to indicate a list of $m$.

The raw terms of the language are $m$ and defined by the EBNF:
\begin{align*}
  m &:= \, x \, | \, r \in \mathbb{R}\setminus \{0\} \, | \, 0\\
    &\quad  m + m \, | \, f([m]) \, (f\in \Sigma) \, | \, d m /d[p] ([m];[m]) \, \\ 
    &\quad (m,m) \, | \, \mathsf{fst}(m) \, | \, \mathsf{snd}(m) \, | \, ()
\end{align*}

In the above, $\Sigma$ is a set of built-in functions, this can include 
items like $\sin$, $\cos$, multiplication, and whatever basic smooth functions 
we require.  To make $\LT$ concrete we will assume it contains $\sin$, $\cos$, and multiplication.
For typing we write $f \in \Sigma(A_1,\ldots,A_n;B)$ to denote that as a symbol $f$ has $n$ inputs of the types
$A_1,\ldots,A_n$ respectively, and produces an output of type $B$.  For example, $\sin,\cos\in \Sigma(\R;\R)$ and 
$\_ \cdot \_ \in \Sigma(\R,\R;\R)$.

In the above $x$ ranges over the names of a countable set of variables.
In the above, $p$ denotes a pattern:
  \[
   p := \, x \, | \, () \, | \, (p,p)    
  \]
Thus, we can differentiate with respect to a list of patterns.

Finally, types are described by the EBNF:
  \[
    \ty := () \, | \, \R \, | \, (\ty,\ty)  
  \]

\section{Typing}
This section lists the typing rules for $\LT$.  For each grammar production rule, there will be a 
typing rule that shows how to make the constructed entity well-typed.  There are also additional pattern rules
to handle the fact that we allow variable patterns.

Terms in $\LT$ are given types in a context of typed patterns.  A context is essentially a record of 
the types of all the variables in the term.  Formally a \textbf{context} is a map whose entries are of the form $p:\ty$ where $p$ is a 
pattern and $\ty$ is a type and $:$ is a symbol that is read $p$ has type $\ty$.  For example, $x:(\R,\R), (y,z):((),\R)$ is a context; 
in this context, we can deduce that $x : (\R,\R)$, $y:()$ and $z:\R$.  However, not all patterns are well-formed.
For example, $x:\R,x:\R$ is a malformed context because a context must list each variable once.  Also, $(x,y) : \R$
is malformed because a pair of variables cannot be of type $\R$.

Table \ref{table:lt-ctx} contains the rules for a well-formed context.  The rules from top to bottom left to right 
are as follows.  The first says that the empty context is well-formed.  The second says that a single variable 
with any type in a singleton context is well-formed.  The third says that given two well-formed context with 
an empty intersection (no overlap on variable names), their concatenation is a well-formed context.
The fourth rule says that a context may always be extended with a declaration of $():()$.  Note, the second rule 
already allows $z:()$ to be added; this rule allows referring to the explicit empty pattern.  Finally, 
the fifth rule says that a context with typed patterns $p:A,q:B$ can be tupled to create a pattern $(p,q):(A,B)$
in context.

\begin{table} 
  \begin{center}
  \fbox{ 
    \begin{tabular}{lr}
      \infer{ \ctx}{}
        & \infer{x:A \ctx}{} \\~\\
      \multicolumn{2}{c}{\infer{\Gamma_1,\Gamma_2 \ctx}{\Gamma_1 \ctx & \Gamma_2 \ctx & \Gamma_1\cap \Gamma_2 = \emptyset}}\\~\\
      \infer{\Gamma,():() \ctx}{\Gamma \ctx}  
        &\infer{\Gamma,(p,q):(A,B) \ctx}{\Gamma,p:A,q:B \ctx}
    \end{tabular}
  }%fbox
  \end{center}
  \caption{Context rules for $\LT$}
  \label{table:lt-ctx}
\end{table}


Table \ref{table:lt-types} contains the rules for typing terms of $\LT$.  Note that the differentiation rule 
is a binding rule as the differential term binds the $p_i$ that are free in $m$ to the term.

\begin{table} 
  \begin{center}
  \fbox{ 
    \begin{tabular}{lr}
      \infer{\Gamma,():() \proves m : B}{\Gamma \proves m :B} 
        & \infer{\Gamma, (p,q):(A,B) \proves m : C}{\Gamma,p:A,q:B \proves m :C}\\~\\
      \infer{\Gamma,p:A \proves p:A}{\Gamma,p:A \ctx} 
        & \infer{\Gamma \proves r:\R}{r \in \R\setminus \{0\}} \\~\\
      \infer{\Gamma \proves 0:A}{} 
        & \infer{\Gamma \proves m + n : B}{\Gamma \proves m: B & \Gamma \proves n :B}\\~\\
      \multicolumn{2}{c}{\infer{\Gamma \proves f(m_1,\ldots,m_n):B}{\{\Gamma \proves m_i:A_i\}_{i=1}^{n} & f\in \Sigma(A_1,\ldots,A_n;B) }}\\~\\
      \infer{\Gamma \proves ():()}{}
        & \infer{\Gamma \proves (m,n):(A,B)}{\Gamma \proves m :A & \Gamma \proves n :B}\\~\\
      \infer{\Gamma \proves \fst(m): A}{\Gamma \proves m:(A,B)}
        & \infer{\Gamma \proves \snd(m): B}{\Gamma \proves m : (A,B)}\\~\\
      \multicolumn{2}{c}{
        \infer{
          \Gamma \proves \diff{m}{p_1,\ldots,p_n}{a_1,\ldots,a_n}{v_1,\ldots,v_n} : B
        }{
          \Gamma,p_1:A_1,\ldots,p_n:A_n \proves m : B
          & \{\Gamma \proves a_i:A_i\}_{i=1}^{n}
          & \{\Gamma \proves v_i:A_i\}_{i=1}^{n}
        }
      }
    \end{tabular}
  }%fbox
  \end{center}
  \caption{Typing rules for $\LT$}
  \label{table:lt-types}
\end{table}


\section{Substitution}
Terms in $\LT$ are cut-free, and we define substitution as an operation on cut-free terms.  Substitution must be 
well typed, the rule for an admissable substitution is:

\[
  \infer{\Gamma \proves m[n/p]:B}{\Gamma \proves n:A & \Gamma,p:A \proves m:B}
\]

This is read $m$ with $n$ substituted for $p$.

The following rewrite rules describe how to move substitutions onto subterms of a term.  The first two rules 
turn subsitution for patterns into substitution for variables.  Afterwards, there is at least one rule for each of the 
term constructions.  The differential binds variables in the term, so to avoid capture, we freshen the bound variables before substituting 
into the term being differentiated.  To make the rule easy to write, we use the notation $p \sim q$ with $q$ fresh to denote 
that $q$ has exactly the same shape as $p$ but where each variable occurrence is freshened.

\begin{enumerate}[{\bf Cut.1}]
  \item $m[n/()] \rewrites m$;
  \item $m[n/(p,q)] \rewrites m[\fst(n)/p][\snd(n)/q]$;
  \item $y[n/x] \rewrites y$ if $y\ne x$;
  \item $y[n/y] \rewrites n$;
  \item $r[n/p] \rewrites r$ where $r \in \R$;
  \item $0[n/p] \rewrites 0$;
  \item $(m+t)[n/p] \rewrites (m[n/p]) + (t[n/p])$;
  \item $()[n/p] \rewrites ()$;
  \item $(m,t)[n/p] \rewrites (m[n/p],t[n/p])$;
  \item $\fst(m)[n/p] \rewrites \fst(m[n/p])$ and $\snd(m)[n/p] \rewrites \snd(m[n/p])$;
  \item $f(m_1,\ldots,m_k)[n/p] \rewrites f( (m_1[n/p]),\ldots,(m_k[n/p]) )$;
  \item For each $i$, let $p_i \sim q_i$ with $q_i$ fresh:
    \begin{align*}
      & \left(\diff{m}{p_1,\ldots,p_k}{a_1,\ldots,a_k}{v_1,\ldots,v_k}\right)[n/p]\\
      & \rewrites \diff{m[q_i/p_i]_{i=1}^{k}[n/p]}{q_1,\ldots,q_n}{a_1[n/p],\ldots,a_k[n/p]}{v_1[n/p],\ldots,v_k[n/p]}
    \end{align*}
\end{enumerate}

To make substitution into an operation on terms we have to make sure it defines a total function.
This is provided by the following Lemma.
\begin{lemma}
  The rewrite rules for substitution form a terminating, confluent rewriting system.
\end{lemma}

Given the rule for cut, one might worry about $\alpha$-equivalence.  However, in the next section 
we will provide the equations on the derivative and see that $\alpha$-equivalence follows from the 
differentiation rules.  For now we state the following.

\begin{lemma}
  Cut, weakening, contraction, and exchange are admissable in $\LT$.
\end{lemma}

\section{Source transformation differentiation}

In this section, we give the source transformation rules for differentiation removal.  We will see 
that under these rules, every term can be transformed into one in which no derivatives occur --- that is, 
the derivative can be eliminated.  However, in order to perform this we need to know that the language 
is closed to derivatives.  This requires a construction on our generating function symbols, so that for example,
we can say what the derivative of $\sin$ is.

\begin{definition}
  An \textbf{$\LT$ differential closure}
  of $\Sigma$ is an assignment for every $f\in \Sigma(A_1,\ldots,A_n;B)$ and every $1 \leq i \leq n$
   to a chosen term in context $p_1:A_1,\ldots,p_n:A_n,v:A_i \proves f_D^i$.
\end{definition}

We think of the $\LT$ differential closure of $\Sigma$ as providing the derivative with respect to each variable 
of every generating function symbol in $\Sigma$.  In other words:

\[
    x_1:A_1,\ldots,x_n:A_n,v:A_i \proves \diff{f(x_1,\ldots,x'_i,\ldots,x_n)}{x'_i}{x_i}{v} = f_D^i
\]

For example, we have 
  \begin{itemize}
    \item $\sin_D^1 = x:\R,v:\R \proves \cos(x)\cdot v:\R$;
    \item $\cos_D^1 = x:\R,v:\R \proves (-1)\cdot \sin(x)\cdot v$;
    \item $\cdot_D^1 = x:\R,y:\R,v:\R \proves v\cdot y$;
    \item $\cdot_D^2 = x:\R,y:\R,v:\R \proves x\cdot v$.
  \end{itemize}

We will use the following notation for working with the $f_D^i$ terms.  Given $f\in \Sigma(A_1,\ldots,A_n;B)$ 
we assume that each $x_i$ and $v$ is fresh in the definition of $f_D^i$ and then define

\[
  f_D^i(a_1,\ldots,a_n;w) := f_D^i[a_i/x_i]_{i=1}^n[w/v]
\]

Indeed taken with the rules of differentiation below this allows 
\[
  \diff{x\cdot y}{x,y}{a,b}{v,w} 
  \rewrites \cdot_D^1(a,b,v) + \cdot_D^2(a,b,w) = v\cdot b + a \cdot w  
\]
as usual.

With this, the rules for differentiation are as follows:

\begin{enumerate}[{\bf [Dt.1]}]
    \item $\diff{m}{}{}{} \rewrites 0$ and \\~
          \begin{align*}
            & \diff{m}{p_1,p_2,\ldots,p_n}{a_1,\ldots,a_n}{v_1,\ldots,v_n}\\
            &\rewrites \diff{m[a_1/p_1]}{p_2,\ldots,p_n}{a_2,\ldots,a_n}{v_2,\ldots,v_n}
               + \diff{m[a_2/p_2]\cdots[a_n/p_n]}{p_1}{a_1}{v_1}
          \end{align*}
    \item $\diff{m}{()}{a}{v} \rewrites 0$ and $\diff{m}{(p,q)}{(a_1,a_2)}{(v_1,v_2)} \rewrites \diff{m}{p,q}{a_1,a_2}{v_1,v_2}$;
    \item $\diff{m}{x}{a}{v} \rewrites 0$ if $x \not \in m$;
    \item $\diff{x}{x}{a}{v} \rewrites v$;
    \item $\diff{m+n}{p}{a}{v} \rewrites \diff{m}{p}{a}{v} + \diff{n}{p}{a}{v}$;
    \item $\diff{(m,n)}{p}{a}{v} \rewrites \left(\diff{m}{p}{a}{v},\diff{n}{p}{a}{v}\right)$;
    \item $\diff{\fst(m)}{p}{a}{v} \rewrites \fst\left(\diff{m}{p}{a}{v}\right)$ and 
          $\diff{\snd(m)}{p}{a}{v} \rewrites \snd\left(\diff{m}{p}{a}{v}\right)$;
    \item 
          \[
              \diff{f(m_1,\ldots,m_n)}{p}{a}{v}
              \rewritesl \sum_{i=1}^n f_D^i(m_1[a/p],\ldots,m_n[a/p];\diff{m_i}{p}{a}{v})
          \]
\end{enumerate}
\textbf{[Dt.1,2]} are the partial derivative splitting rules: they show how to break total differentiation 
into a sum of partials.  These two rules allow to push the derivative down onto variables.  Then we have at least 
one differentiation rule for each of the term constructions except for the derivative.  We do not handle differentiation 
of the derivative here.  Instead the full differentiation removal is applied to the inner derivative, and the resulting 
term is differentiated.

Some of the rewrite rules introduce substitutions; all such substitutions are well-typed.  This follows 
from the more general fact that differentiation transformation is type preserving.

\begin{lemma}
  For each of the rewrite rules \textbf{[Dt.1--8]}, if $\Gamma \proves m : B$ and $m\rewrites m'$ then $\Gamma \proves m':B$.
\end{lemma}

Differentiation removal is strongly normalizing.

\begin{lemma}
  The differential transformation rules \textbf{[Dt.1--8]} are strongly normalizing.
\end{lemma}


\end{document}