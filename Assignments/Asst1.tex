\documentclass{article}

\title{Differential Languages: Assignment 1}
\author{}
% \author{Robin Cockett and Jonathan Gallagher}

\usepackage{proof}
\usepackage{amsmath,amssymb,amsfonts,amsthm}
\usepackage{enumerate}


% Nullary 
\newcommand{\R}{\mathcal{R}}
\newcommand{\proves}{\vdash}
\newcommand{\LT}{\mathsf{L}_T}
\newcommand{\fst}{\mathsf{fst}}
\newcommand{\snd}{\mathsf{snd}}
% Tetrary 
\newcommand{\diff}[4]{\frac{\mathsf{d}\, {#1}}{\mathsf{d}\, {#2}}\left({#3};{#4}\right)}
\begin{document}
\maketitle
 \newcounter{mycounter}
 \stepcounter{mycounter}
 
\section*{Question \arabic{mycounter}}
\stepcounter{mycounter}
Using the formal derivative rules, compute the derivative with  respect to $x$ of the following program:
  \[
        \sin(x\cdot y)\cdot x^2
  \]
To do this, complete the following steps:
\begin{enumerate}
    \item Write the above program as a \emph{term-in-context}, and make sure it is well-typed.
    \item Write down the term-in-context for the derivative of the program where the point and direction are contextual variables.
          As a sanity check, your context should have three variables in it.
\end{enumerate}

\section*{Question \arabic{mycounter}}
\stepcounter{mycounter}
Using the formal derivative rules, compute the derivative with respect to $(x,(y,z))$ of the following program:
  \[
    x:\R,(y,z):(\R,\R) \proves ((x \cdot (\sin(\cos(y\cdot z)\cdot x^2+xy)) , \sin(\cos(\sin(\cos(x\cdot y \cdot z))))), x\cdot y^5+y\cdot z^4) : ((\R,\R),\R)
  \]
Also, using the formal derivative rules, compute the derivative with respect to only $(x,z)$.
 
\section*{Question \arabic{mycounter}}
\stepcounter{mycounter}
This question relates the programming language we've developed to differential functions of the 
form $\R^n \to \R^m$.  Recall that the types in $\LT$ are of the form
  \[\mathsf{ty} := () \, |\, \R\, |\, (\mathsf{ty},\mathsf{ty}) \]
Represent $\R^n$ in these types by induction:
  \begin{align*}
    \R^0 &= () \\ 
    \R^1 &= \R \\ 
    \R^{n+1} &= (\R^n,\R)
  \end{align*}
For example, $\R^3 = ((\R,\R),\R)$.  A type $A$ is called \emph{flattened} when it is of the form $R^m$ for some $m$.
We will refer to an $m$-tuple as an element of $\R^m$ and write it as $((x_1,x_2),\ldots,x_m)$ and sometimes just $(x_1,\ldots,x_m)$.

Every type can be flattened.  The \emph{length} of a type $A$, denoted $|A|$, is the number of $\R$s that appear in it.
For example $|(\R,(\R,\R))| =3$.  For each type $A$, construct a program $x:A \proves \mathsf{flat}_A : \R^{|A|}$ in 
a way that retains all the information, in order, from the original type.  As an example $p:(\R,((),\R)) \proves (\fst(p),\snd(\snd(p))) : \R^2$.

Given a type $\mathsf{A}$, define a program in $\LT$ that implements $\mathsf{flat}_A$.  Note, the implementation 
depends on the type $\mathsf{A}$, and also you may find it convenient to define helper programs.

\section*{Question \arabic{mycounter}}
\stepcounter{mycounter}
For each type $A$, define a program that implements scalar multiplication: that is define $\cdot_A$ (or in prefix notation $\cdot_A(r,x)$):
\[
      r:\R,x:A \proves r \cdot_A x
\]

Show that for flattened types $\cdot_A$ has the following shape

\[
  r \cdot_{(A,\R)} (v,s) = (r\cdot_A v,r\cdot s)
\]

\section*{Question \arabic{mycounter}}
\stepcounter{mycounter}
This question relates the Jacobian matrix, the total derivative, the gradient and our 
formal rules for differentiation.  

Let $f$ be a smooth function $\R^n \to \R^m$.  Then using the notion of flattened types introduced above,
$f$ is an $m$-tuple of functions $f = ((f_1,f_2),\ldots,f_m)$, and each $f_j$ is a function whose input is an 
$n$-tuple $((x_1,x_2),\ldots,x_n)$.  This means that each $f_j$ may be regarded as a program $x_1:\R,\ldots,x_n:\R \proves f_j :\R$.


The Jacobian of $f$ at a point $a=((a_1,a_2),\ldots,a_n) : \R^n$ is the matrix $J_f(a)$ where 
the $i,j$-component (the component in the $i^\text{th}$ row and $j^\text{th}$ column) is 
\[
    \diff{f_i[a_k/x_k]_{k\ne j}}{x_j}{a_j}{1}
\]

The total derivative of $f$ (denoted $D[f]$) is defined to be the matrix product of the Jacobian $J_f(a)$ with a directional 
vector $v$:  $D[f](a;v) = J_f(a)\cdot v$.

When $m=1$, the Jacobian at $a$ is called the gradient at $a$.  In this case the matrix-product reduces to the dot product.

\begin{enumerate}
    \item Give an explicit description of the Jacobian matrix for functions $\R^0 \to \R^m$.
    \item Give an explicit description of the Jacobian matrix for functions $\R^n \to \R^0$.
    \item Assume $n,m > 0$ and write out the Jacobian matrix (using $\cdots$) for function $\R^n \to \R^m$.
    \item Write the Jacobian for the functions defined in questions \emph{1} and \emph{2}.
    \item Show that in general, the total derivative $D[f](a;v)$ is exactly $\diff{f}{(x_1,\ldots,x_n)}{a}{v}$.  Before answering, what shape do $a,v$ have to have?
    \item Show, in generality, given any $g : \R^n \to \R$ that the gradient of $g$ is $\diff{g}{(x_1,\ldots,x_n)}{a}{(1,\ldots,1)}$.
          Show in fact that the gradient of $g$ at $a$ is the unique vector in $\R^n$ such that for any $v\in R^n$ 
          $\diff{g}{(x_1,\ldots,x_n)}{a}{(1,\ldots,1)} \cdot v = \diff{g}{(x_1,\ldots,x_n)}{a}{v}$ (where the $\cdot$ is the dot product).
    \item Show that the $i^\text{th}$ row of the Jacobian matrix is the gradient of $f_i$.
\end{enumerate}

\section*{Question \arabic{mycounter}}
\stepcounter{mycounter}
Compute the derivative of the following using the formal derivative rules:

  \[
      \diff{\diff{\sin(x\cdot y)\cdot \cos(x\cdot z)}{(x,z)}{(a_1,a_2)}{(v_1,v_2)}}{(y,a_1,v_2)}{(b,c,d)}{(u,w,t)}
  \]

\section*{Question \arabic{mycounter}}
\stepcounter{mycounter}
Complete the following activities:
\begin{enumerate}
    \item Sign-up for a gitlab account if you don't have one already and send your username to Jonathan.  Add an ssh key to your account.
    \item Clone the Language-Test repository.
    \item Install the Haskell tool stack.
    \item Run the build script, and attach a screen shot of the result.
    \item Download VSCode and the (official) Haskell plugin.
    \item Add your name to Readme.md, stage, commit and push the change on git.
\end{enumerate}
\end{document}